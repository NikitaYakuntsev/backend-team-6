package ru.surf.hackathon.backend.service;

import ru.surf.hackathon.backend.dto.LampInfoDTO;

public interface LampService {
    LampInfoDTO getLamp(String barcode);
}
