package ru.surf.hackathon.backend.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.surf.hackathon.backend.dto.LampInfoDTO;
import ru.surf.hackathon.backend.entity.Lamp;
import ru.surf.hackathon.backend.exception.InvalidBarCodeException;
import ru.surf.hackathon.backend.exception.LampNotFoundException;
import ru.surf.hackathon.backend.repository.LampRepository;
import ru.surf.hackathon.backend.service.LampService;
import ru.surf.hackathon.backend.utils.BarcodeUtils;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class LampServiceImpl implements LampService {
    public static final String imagePath = "https://lamptest.ru/images/photo/";
    private final LampRepository lampRepository;

    @Transactional
    public LampInfoDTO getLamp(String barCode) {
        if (!BarcodeUtils.validateBarcode(barCode)) {
            throw new InvalidBarCodeException();
        }
        Lamp lamp = lampRepository.findByBarcode(barCode)
                .orElseThrow(LampNotFoundException::new);

        return LampInfoDTO.builder()
                .brand(lamp.getBrand())
                .lm(lamp.getLm())
                .p(lamp.getP())
                .colorL(lamp.getColorL())
                .base(lamp.getBase())
                .imageUrl(Objects.isNull(lamp.getImage()) ? "" : imagePath + lamp.getImage() + ".jpg")
                .rating(lamp.getRating())
                .price(getPrice(lamp))
                .build();
    }

    private String getPrice(Lamp lamp) {
        if (Objects.isNull(lamp.getRub()) && Objects.isNull(lamp.getUsd())) {
            return "";
        }
        return Objects.nonNull(lamp.getRub()) ? lamp.getRub().toString() : "$" + lamp.getUsd();
    }
}
