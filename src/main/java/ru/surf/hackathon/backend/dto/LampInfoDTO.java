package ru.surf.hackathon.backend.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LampInfoDTO {

    @JsonProperty(value = "brand")
    private String brand;

    @JsonProperty(value = "lm")
    private Integer lm;

    @JsonProperty(value = "power")
    private BigDecimal p;

    @JsonProperty(value = "color")
    private String colorL;

    @JsonProperty(value = "base")
    private String base;

    @JsonProperty(value = "image_url")
    private String imageUrl;

    @JsonProperty(value = "rating")
    private BigDecimal rating;

    @JsonProperty(value = "price")
    private String price;
}
