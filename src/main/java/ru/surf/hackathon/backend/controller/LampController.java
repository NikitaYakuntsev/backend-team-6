package ru.surf.hackathon.backend.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.surf.hackathon.backend.dto.LampInfoDTO;
import ru.surf.hackathon.backend.dto.ErrorDTO;
import ru.surf.hackathon.backend.service.impl.LampServiceImpl;

@RestController
@RequiredArgsConstructor
@Tag(name = "Lamps")
@RequestMapping("/lamps")
public class LampController {
    private final LampServiceImpl lampServiceImpl;

    @Operation(summary = "Returns an information about lamp by it's barcode")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Operation succeeded",
                    content = @Content(schema = @Schema(implementation = LampInfoDTO.class))),
            @ApiResponse(responseCode = "400",
                    description = "Invalid barcode",
                    content = @Content(schema = @Schema(implementation = ErrorDTO.class))),
            @ApiResponse(responseCode = "404",
                    description = "Lamp not found",
                    content = @Content(schema = @Schema(implementation = ErrorDTO.class)))})
    @GetMapping(value = "/{barCode}", produces = MediaType.APPLICATION_JSON_VALUE)
    public LampInfoDTO getLamp(@Parameter(name = "barCode", description = "barcode of lamp to be searched")
                               @PathVariable("barCode") String barCode) {
        return lampServiceImpl.getLamp(barCode);
    }
}
