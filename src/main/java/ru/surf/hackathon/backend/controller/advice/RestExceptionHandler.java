package ru.surf.hackathon.backend.controller.advice;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.surf.hackathon.backend.dto.ErrorDTO;
import ru.surf.hackathon.backend.exception.AppException;

@RestControllerAdvice
public class RestExceptionHandler {
    @ExceptionHandler(value = {AppException.class})
    public ResponseEntity<ErrorDTO> handleAppException(AppException exception) {
        return ResponseEntity.status(exception.getStatusCode())
                .body(new ErrorDTO(exception.getMessage()));
    }
}
