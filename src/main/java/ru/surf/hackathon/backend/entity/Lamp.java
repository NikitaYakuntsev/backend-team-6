package ru.surf.hackathon.backend.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Lamp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "no")
    private Long id;

    private String brand;

    private String model;

    @Column(name = "power_l")
    private BigDecimal powerL;

    private Integer matt;

    private BigDecimal dim;

    @Column(name = "color_l")
    private String colorL;

    private String lm_l;

    private String eq_l;

    private String ra_l;

    private String u;

    private String pf_l;

    @Column(name = "angle_l")
    private String angleL;

    private Integer life;

    private String war;

    private String prod;

    private Integer w;

    private Integer d;

    private Integer h;

    private Integer t;

    private String barcode;

    private String plant;

    private String base;

    private String shape;

    //TODO LIST types
    private String type;
    private String type2;

    private String url;

    private String shop;

    private BigDecimal rub;

    private BigDecimal usd;

    private BigDecimal p;

    private BigDecimal pf;

    private Integer lm;

    private Integer color;

    private BigDecimal cri;

    private Integer r9;

    @Column(name = "Rf")
    private BigDecimal rf;

    @Column(name = "Rg")
    private BigDecimal rg;

    @Column(name = "Duv")
    private BigDecimal duv;

    private BigDecimal flicker;

    private Integer angle;

    @Column(name = "switch")
    private Integer switcher;

    private BigDecimal umin;

    private Integer drv;

    private Integer tmax;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private LocalDate date;

    private Long instruments;

    private String add2;

    private String add3;

    private String add4;

    private String add5;

    private Integer cqs;

    private Integer eq;

    private BigDecimal rating;

    private Integer act;

    @Column(name = "lamp_image")
    private String image;

    @Column(name = "lamp_desc")
    private String description;
}
