package ru.surf.hackathon.backend.exception;

import org.springframework.http.HttpStatus;

public class InvalidBarCodeException extends AppException {
    public InvalidBarCodeException() {
        super("Invalid barcode", HttpStatus.BAD_REQUEST);
    }
}
