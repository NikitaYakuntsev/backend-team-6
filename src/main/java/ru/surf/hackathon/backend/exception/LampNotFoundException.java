package ru.surf.hackathon.backend.exception;

import org.springframework.http.HttpStatus;

public class LampNotFoundException extends AppException {
    public LampNotFoundException() {
        super("Lamp not found", HttpStatus.NOT_FOUND);
    }
}
