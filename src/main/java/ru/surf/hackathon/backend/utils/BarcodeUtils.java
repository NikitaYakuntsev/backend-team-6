package ru.surf.hackathon.backend.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BarcodeUtils {
    private static final Pattern barcodePattern = Pattern.compile("[0-9]{13}");

    private BarcodeUtils() {
    }

    public static boolean validateBarcode(String barcode) {
        Matcher matcher = barcodePattern.matcher(barcode);
        return matcher.matches();
    }
}
